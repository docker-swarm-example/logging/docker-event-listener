# Docker Event Listener
The Docker event listener will listen for [container events](https://docs.docker.com/engine/reference/commandline/events/#object-types) sent from the Docker daemon and forward them to [Fluentd](https://www.fluentd.org) for further processing.

## Prerequisites
This project assumes that you have Fluentd deployed in your Docker swarm. You can use following [project](https://gitlab.com/docker-swarm-example/logging/efk-logging) to get Fluentd up and running in your Docker swarm.

## Deploy

Execute the following commands to deploy [Docker Event Listener](https://gitlab.com/docker-swarm-example/logging/docker-event-listener) to Docker swarm:

```sh
git clone https://gitlab.com/docker-swarm-example/logging/docker-event-listener.git
docker stack deploy --compose-file docker-event-listener/docker-compose.yml logging
```
