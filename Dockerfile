##### Dependencies #####
FROM node:10-slim as dependencies

WORKDIR /cache

COPY package*.json ./

# Install production dependencies
RUN npm ci --only=production
RUN cp -R node_modules prod_node_modules

# Install dev dependencies
RUN npm install

##### Lint #####
FROM node:10-slim as lint

WORKDIR /usr/src/app
COPY --from=dependencies /cache/node_modules node_modules
COPY package.json .
COPY tslint.json .
COPY tsconfig.json .
COPY src src
RUN npm run lint

##### Build #####
FROM node:10-slim as builder

WORKDIR /usr/src/app
COPY --from=dependencies /cache/node_modules ./node_modules
COPY package.json .
COPY tsconfig.json ./
COPY src src
RUN npm run build

##### Final Build #####
FROM node:10-alpine
WORKDIR /usr/src/app
COPY --from=dependencies /cache/prod_node_modules ./node_modules
COPY --from=builder /usr/src/app/build .

EXPOSE 8080
CMD [ "node", "index.js" ]
