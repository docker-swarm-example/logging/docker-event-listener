import eventHandler from './event-handler';
import { logger } from './logger';
import dockerode from 'dockerode';
import dockerEvents from 'docker-events';

const emitter = new dockerEvents({
  docker: new dockerode(),
});

emitter.start();

emitter.on('connect', () => {
  console.log('Listening for docker events.');
});

emitter.on('disconnect', () => {
  console.log('Stopped listening for docker events.');
});

emitter.on('create', eventHandler);
emitter.on('start', eventHandler);
emitter.on('stop', eventHandler);
emitter.on('die', eventHandler);
emitter.on('destroy', eventHandler);
