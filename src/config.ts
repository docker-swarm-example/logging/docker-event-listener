export default {
  FLUENTD_HOST: process.env.FLUENTD_HOST || 'fluentd',
  FLUENTD_PORT: process.env.FLUENTD_HOST || 24224,
  FLUENTD_TIMEOUT: process.env.FLUENTD_HOST || 3.0,
  FLUENTD_RECONNECT_INTERVAL: process.env.FLUENTD_HOST || 60000,
};
