import { logger, EventTime } from './logger';

export default (event) => {
  console.log(
    'Received event %s from container %s',
    event.status,
    event.Actor.Attributes.name,
  );

  const time = getEventTime(event);

  logger.emit(event.status, event, time);
};

function getEventTime(event) {
  const nanoSeconds = event.timeNano - (event.time * 1000000000);
  return new EventTime(event.time, nanoSeconds);
}
