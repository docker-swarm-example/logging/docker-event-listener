import config from './config';
const logger = require('fluent-logger');
const EventTime = logger.EventTime;

logger.configure('docker-container-event', {
  host: config.FLUENTD_HOST,
  port: config.FLUENTD_PORT,
  timeout: config.FLUENTD_TIMEOUT,
  reconnectInterval: config.FLUENTD_RECONNECT_INTERVAL,
});

export {
  logger,
  EventTime,
};
